package com.example.homework16roomdatabase.ui.homefragment.homefragment

import androidx.lifecycle.ViewModel
import com.example.homework16roomdatabase.database.AppDatabase
import com.example.homework16roomdatabase.model.Items
import kotlinx.coroutines.flow.Flow

class HomeViewModel : ViewModel() {

    val items : Flow<List<Items>> = AppDatabase.db.getItemsDao().getAll()

}