package com.example.homework16roomdatabase.ui.homefragment.editHomeFragment

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.homework16roomdatabase.databinding.EditHomeFragmentBinding
import com.example.homework16roomdatabase.ui.homefragment.BaseFragment
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditHomeFragment : BaseFragment<EditHomeFragmentBinding>(EditHomeFragmentBinding::inflate) {

    private lateinit var text: String

    private val viewModel: EditHomeViewModel by viewModels()

    override fun start() {
        observes()
        listeners()

    }

    private fun listeners() {
        val title = binding.titleET.text.toString()
        val descripton = binding.descriptionTV.text.toString()
        val url = binding.urlET.text.toString()
        binding.addButton.setOnClickListener {
            addItems(title, descripton, url)
        }
    }

    private fun addItems(title: String, descripton: String, url:String ) {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(IO){
                viewModel.addItems(title, descripton, url)
                Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun observes() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.response.collect {
                    text = it.toString()
                }
            }
        }
    }


}