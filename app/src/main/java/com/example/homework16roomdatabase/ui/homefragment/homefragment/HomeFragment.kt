package com.example.homework16roomdatabase.ui.homefragment.homefragment

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework16roomdatabase.R
import com.example.homework16roomdatabase.adapter.ItemsAdapter
import com.example.homework16roomdatabase.databinding.HomeFragmentBinding
import com.example.homework16roomdatabase.model.Items
import com.example.homework16roomdatabase.ui.homefragment.BaseFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeFragment : BaseFragment<HomeFragmentBinding>(HomeFragmentBinding::inflate) {

    private lateinit var adapter: ItemsAdapter

    private val viewModel: HomeViewModel by viewModels()

    override fun start() {

        binding.addButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editHomeFragment)
        }

        initRecyclerView()
        observes()
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter = ItemsAdapter()
        binding.recyclerView.adapter = adapter
    }

    private fun observes() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.items.collect {
                    if (it.isNotEmpty()) {
                        binding.empty.visibility = View.GONE
                        binding.recyclerView.visibility = View.VISIBLE
                        adapter.setData(it as MutableList<Items>)

                    }else{
                        binding.empty.visibility = View.VISIBLE
                        binding.recyclerView.visibility = View.GONE
                    }
                }
            }
        }
    }


}