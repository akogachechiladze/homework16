package com.example.homework16roomdatabase.ui.homefragment.editHomeFragment

import androidx.lifecycle.ViewModel
import com.example.homework16roomdatabase.Resource
import com.example.homework16roomdatabase.database.AppDatabase
import com.example.homework16roomdatabase.model.Items
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

class EditHomeViewModel() : ViewModel() {

    var response = MutableSharedFlow<Resource>()

    suspend fun addItems(title: String, description: String, url: String) {

        if (title.length in 5..29 && description.length in 32..300 && url.isNotEmpty()) {
            AppDatabase.db.getItemsDao().insertAll(Items(0, title, description, url))
            response.emit(Resource.Success("Item added"))

        }else{
            response.emit(Resource.Failure("Check fields requirements"))
        }
    }

}