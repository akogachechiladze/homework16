package com.example.assignment9.setImage

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.homework16roomdatabase.R


fun ImageView.setImage(url: String?) {
    if(!url.isNullOrEmpty()) {
        Glide.with(this).load(url).placeholder(R.mipmap.blackscreen).into(this)
    }
    else{
        setImageResource(R.mipmap.blackscreen)
    }
}