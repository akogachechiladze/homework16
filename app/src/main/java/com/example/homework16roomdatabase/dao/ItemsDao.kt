package com.example.homework16roomdatabase.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.homework16roomdatabase.model.Items
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemsDao {
    @Query("SELECT * FROM items")
    fun getAll(): Flow<List<Items>>

    @Insert
    fun insertAll(users: Items)

    @Delete
    fun delete(user: Items)
}