package com.example.homework16roomdatabase.database


import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.homework16roomdatabase.Application
import com.example.homework16roomdatabase.dao.ItemsDao
import com.example.homework16roomdatabase.model.Items

@Database(entities = [Items::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getItemsDao(): ItemsDao


    companion object {

        val db = Room.databaseBuilder(
            Application.appContext!!,
            AppDatabase::class.java, "database-name"
        ).build()

    }
}