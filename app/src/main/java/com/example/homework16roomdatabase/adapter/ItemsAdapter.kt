package com.example.homework16roomdatabase.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment9.setImage.setImage
import com.example.homework16roomdatabase.databinding.ItemsLayoutBinding
import com.example.homework16roomdatabase.model.Items
import kotlinx.coroutines.flow.Flow

class ItemsAdapter: RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    private val items = mutableListOf<Items>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: ItemsLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Items
        fun onBind() {
            binding.apply {
                titleTV.text = model.title
                descriptionTV.text = model.description
                coverIV.setImage(model.url)
            }
        }
    }

    fun setData(items: MutableList<Items>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()

    }

}