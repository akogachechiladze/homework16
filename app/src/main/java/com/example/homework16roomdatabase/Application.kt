package com.example.homework16roomdatabase

import android.app.Application
import android.content.Context

class Application: Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }

    companion object {
        var appContext: Context? = null
    }

}