package com.example.homework16roomdatabase


sealed class Resource{
    data class Success(val text: String): Resource()
    data class Failure(val text: String): Resource()
}
